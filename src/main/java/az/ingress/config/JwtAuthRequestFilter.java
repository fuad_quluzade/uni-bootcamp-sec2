package az.ingress.config;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;

@Slf4j
@RequiredArgsConstructor
@Configuration
public class JwtAuthRequestFilter extends OncePerRequestFilter {

    public static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer ";
    private static final String BASIC="Basic ";

//    private  AuthenticationManager authenticateManager;
//    private final JwtCredentials jwtCredentials;

    private final JwtService jwtService;



    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        log.info("jwt auth request filter");
        String authorization = request.getHeader(AUTHORIZATION);
        String token = "";
        String basic="";
        if (authorization !=null && authorization.startsWith(BEARER)) {
            token = authorization.substring(BEARER.length());
            Claims claims = jwtService.parseToken(token);
            Authentication authenticationBearer = jwtService.getAuthenticationBearer(claims);
            SecurityContextHolder.getContext().setAuthentication(authenticationBearer);
        }else if(authorization !=null && authorization.startsWith(BASIC)){
            basic=authorization.substring(BASIC.length());
            byte[] credDecoded = Base64.getDecoder().decode(basic);
            String credentials = new String(credDecoded, StandardCharsets.UTF_8);
            // credentials = username:password
            final String[] values = credentials.split(":", 2);
            User user=new User();
            user.setUsername(values[0]);
            user.setPassword(values[1]);
            Authentication auth = new UsernamePasswordAuthenticationToken(user,null,new ArrayList<>());
//             new SecurityContextImpl(authenticateManager.authenticate(auth));
             SecurityContextHolder.getContext().setAuthentication(auth);

        }
//        request.setAttribute("hello","Hello from usa");
        filterChain.doFilter(request, response);
    }
}
