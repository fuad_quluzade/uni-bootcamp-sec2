package az.ingress;

import az.ingress.domain.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
@EnableKafka
@Slf4j
@RequiredArgsConstructor
public class UniBootcampSec2Application {

    private final ObjectMapper objectMapper;

    public static void main(String[] args) {
        SpringApplication.run(UniBootcampSec2Application.class, args);
    }



    @KafkaListener(topics="register-topic",containerFactory = "jsonKafkaListenerContainerFactory")
    public void listener(ConsumerRecord<String,String> data) throws JsonProcessingException {
        log.info("message from producer {}", data);
        User publisherDto = objectMapper.readValue(data.value(), User.class);
        log.info("publisher dto is {}", publisherDto);
    }
}
