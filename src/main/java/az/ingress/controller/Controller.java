package az.ingress.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class Controller {

    @GetMapping("/hello")
    public String permitAll(){
        return "hello";
    }

    @GetMapping("/onlyForUsers")
    public String onlyForUsers(){
        return "hello from users";
    }


    @GetMapping("/onlyForManagers")
    public String onlyForManagers(){
        return "hello from Managers";
    }
}
